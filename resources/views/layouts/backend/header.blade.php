<style>
    .fa.fa-shopping-cart {
        position: absolute;
        right: 0px;
        top: 0px;
        padding: 9px 12px 7px 10px;
        background: red;
        height: 100%;
        font-size: 130%;
    }

    .add-to-cart {
        transition: 0.4s;
        color: #fff;
        background-color: #303030;
        text-transform: uppercase;
        position: relative;
        padding-right: 52px
    }
</style>

<header class="main-header">
    <!-- Logo -->
    <a href="{{route('Home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>G</b>P-P</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Guest Posting</b> Portal</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                @hasrole('admin')

                @else
                    <li style="margin-top:10px">
                        {{--    <form method="post" enctype="multipart/form-data" action="{{route('blog.cart')}}" >--}}
                        {{--    {{ csrf_field() }}--}}
                        {{--    <input type="hidden" name="url" class="on-load-url" value="{{url('/blogs/add_to_cart_update')}}"/>--}}
                        {{--    <a href="{{route('blog.cart')}}"  class="add-to-cart" >--}}
                        {{--        <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="cart-detail"> </span> item(s)--}}
                        {{--    </a>--}}
                        <input type="hidden" name="url" class="on-load-url"
                               value="{{url('/blogs/add_to_cart_update')}}"/>
                        <a href="{{route('blog.cart')}}" class="btn btn-app bg-navy custom-cart-btn">
                            <span class="badge bg-red cart-detail"> </span>
                            <i class="fa fa-inbox"> <span class="custom-cart"> Cart </span></i>
                        </a>
                        {{-- </form> --}}
                    </li>
                    @endrole

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('backend/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{strtoupper(Auth::user()->name) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset('backend/img/user2-160x160.jpg') }}" class="img-circle"
                                     alt="User Image">
                                <p class="custom-user-header">
                                    {{ ucfirst(Auth::user()->name)  }}
                                    <small>Member
                                        since {{ \Carbon\Carbon::parse(Auth::user()->created_at)->format(' F. Y') }}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{route('user.setting')}}" class="btn bg-navy btn-flat">Setting</a>
                                </div>

                                <div class="pull-right">
                                    <a class="btn bg-navy btn-flat" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    @if (\Request::is('Home'))
                                        working
                                    @endif

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                            <!-- Menu Footer End -->
                        </ul>
                    </li>


            </ul>
        </div>
    </nav>
</header>
