@if(Session::has('flash_message'))

    <section class="content-header">
        <div class="row">
            <div class="col-md-12">

                <div class="custom-alerts alert alert-success"><em> {!! session('flash_message') !!}</em>
                </div>

            </div>
        </div>
    </section>

@endif

@if(Session::has('flash_error_message'))

    <section class="content-header">
        <div class="row">
            <div class="col-md-12">
                <div class="custom-alerts alert alert-danger"><em> {!! session('flash_error_message') !!}</em>
                </div>
            </div>
        </div>
    </section>


@endif

@if ($message = Session::get('success'))

    <section class="content-header">
        <div class="row">
            <div class="col-md-12">

                <div class="custom-alerts alert alert-success" style=" z-index: 100000 !important;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    {!! $message !!}
                </div>
                <?php Session::forget('success');?>

            </div>
        </div>
    </section>

@endif

@if ($message = Session::get('error'))

    <section class="content-header">
        <div class="row">
            <div class="col-md-12">

                <div class="custom-alerts alert alert-danger" style=" z-index: 100000 !important;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    {!! $message !!}
                </div>
                <?php Session::forget('error');?>

            </div>
        </div>
    </section>

@endif





