<!DOCTYPE html>
<html>
@include('layouts.backend.head')
<body class="skin-blue sidebar-mini fixed">


@include('layouts.backend.header')

{{--@if(Session::has('flash_message'))--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-push-3">--}}
{{--                <div class="custom-alerts alert alert-success"><em> {!! session('flash_message') !!}</em>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{--@if(Session::has('flash_error_message'))--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-push-3">--}}
{{--                <div class="custom-alerts alert alert-danger"><em> {!! session('flash_error_message') !!}</em>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{--@if ($message = Session::get('success'))--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-push-3">--}}
{{--                <div class="custom-alerts alert alert-success" style=" z-index: 100000 !important;">--}}
{{--                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>--}}
{{--                    {!! $message !!}--}}
{{--                </div>--}}
{{--                <?php Session::forget('success');?>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{--@if ($message = Session::get('error'))--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-push-3">--}}
{{--                <div class="custom-alerts alert alert-danger" style=" z-index: 100000 !important;">--}}
{{--                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>--}}
{{--                    {!! $message !!}--}}
{{--                </div>--}}
{{--                <?php Session::forget('error');?>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{--@if (count($errors) > 0)--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-push-3">--}}
{{--                <div class="custom-alerts alert alert-danger">--}}
{{--                    <ul>--}}
{{--                        @foreach ($errors->all() as $error)--}}
{{--                            <li>{{ $error }}</li>--}}
{{--                        @endforeach--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}



@include('layouts.backend.nav')

@yield('section')



@include('layouts.backend.script')
@yield('script')

@hasrole('admin')

@else

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5e271bcb8e78b86ed8aa6394/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

@endrole



</body>
</html>
