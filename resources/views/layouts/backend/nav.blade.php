<?php

use App\orders;
use App\keyword;
use App\Industry;
use App\blog;
use App\User;
use App\permissions;
use Spatie\Permission\Models\Role;


$user         = Auth::user();
    $orders       = "";
    $keywords     = "";
    $industries   = "";
    $blogs        = "";
    $permissions  = "";
    $users        = "";
    $roles        = "";

    if($user->hasRole(1)){
        $orders = orders::latest()->get();
    }
    else{
        $current_user_id = Auth::user()->id;
        $orders = orders::where('user_id',$current_user_id)->latest()->get();
    }

    $blogs          = blog::all();
    $keywords       = keyword::all();
    $industries     = Industry::all();
    $users          = User::whereHas('roles', function($q){
                            $q->where('name' ,'!=', 'admin');
                        })->get();
    $permissions    = permissions::all();
    $roles          = Role::all();

?>


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('backend/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{strtoupper(Auth::user()->name) }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
    {{--            <form action="#" method="get" class="sidebar-form">--}}
    {{--                <div class="input-group">--}}
    {{--                    <input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--                    <span class="input-group-btn">--}}
    {{--                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--                    </button>--}}
    {{--                  </span>--}}
    {{--                </div>--}}
    {{--            </form>--}}
    <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->


        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class="@if(\Request::is('home')) active menu-open @endif">
                <a href="{{ route('Home')}}">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>

            @hasrole('admin')

            <li class="treeview @if(\Request::is('users/*')) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-user"></i> <span>User</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(\Request::is('users/allUsers')) active @endif"><a href="{{route('users.allusers')}}"><i
                                class="fa fa-circle-o"></i> All User
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{count($users)}}</small>
                            </span>
                        </a>
                    </li>
                    <li class="@if(\Request::is('users/create')) active @endif"><a href="{{route('users.create')}}"><i
                                class="fa fa-circle-o"></i> Add User</a></li>
                </ul>
            </li>

            <li class="treeview @if(\Request::is('roles/*')) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Roles</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(\Request::is('roles/allRoles')) active @endif"><a
                            href="{{ route('roles.allroles')}}"><i class="fa fa-circle-o"></i> All Roles
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{ count($roles) }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="@if(\Request::is('roles/create')) active @endif"><a href="{{url('roles/create')}}"><i
                                class="fa fa-circle-o"></i> Add Role</a></li>
                </ul>
            </li>
            <li class="treeview @if(\Request::is('permissions/*')) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-key"></i> <span>Permissions</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(\Request::is('permissions/allPermissions')) active @endif"><a
                            href="{{route('permissions.allpermissions')}}"><i class="fa fa-circle-o"></i> All
                            Permissions
                            <span class="pull-right-container">
                                <small class="label pull-right bg-blue">{{ count($permissions)  }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="@if(\Request::is('permissions/create')) active @endif"><a
                            href="{{route('permissions.create')}}"><i class="fa fa-circle-o"></i>Add Permission</a></li>

                </ul>
            </li>
            <li class="treeview @if(\Request::is('keywords/*')) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-font"></i><span>Keywords</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(\Request::is('keywords/allKeywords')) active @endif"><a
                            href="{{route('keywords.allkeywords')}}"><i class="fa fa-circle-o"></i> All Keywords
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{ count($keywords)  }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="@if(\Request::is('keywords/create')) active @endif"><a
                            href="{{route('keywords.create')}}"><i class="fa fa-circle-o"></i> Add keyword</a></li>
                </ul>
            </li>

            <li class="treeview @if(\Request::is('industry/*')) active menu-open @endif">
                <a href="#">
                    <i class="fa fa-industry"></i><span>Industries</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(\Request::is('industry/allIndustries')) active @endif"><a
                            href="{{route('industry.allindustries')}}"><i class="fa fa-circle-o"></i> All Industries
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{ count($industries)  }}</small>
                            </span>
                        </a>
                    </li>
                    <li class="@if(\Request::is('industry/create')) active @endif"><a
                            href="{{route('industry.create')}}"><i class="fa fa-circle-o"></i> Add Industry</a></li>
                </ul>
            </li>
            @endrole

            @hasrole('admin')

            @else
                <li class="@if(\Request::is('blogs/*')) active @endif">
                    <a href="{{route('blogs.allblogs')}}">
                        <i class="fa fa-th"></i>
                        <span>Marketplace</span>
                    </a>
                </li>
                @endrole

                @hasrole('admin')
                <li class="treeview @if(\Request::is('blogs/*')) active menu-open @endif ">
                    <a href="#">
                        <i class="fa fa-th"></i><span>Marketplace</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @hasrole('admin')
                        <li class="@if(\Request::is('blogs/create')) active @endif"><a href="{{route('blogs.create')}}"><i
                                    class="fa fa-circle-o"></i> Add Blog</a></li>
                        <li class="@if(\Request::is('blogs/list')) active @endif"><a href="{{route('blogs.bloglist')}}"><i
                                    class="fa fa-circle-o"></i> Blog List
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{ count($blogs)  }}</small>
                            </span>
                            </a>
                        </li>
                        @else
                            {{--                                    <li class="@if(\Request::is('blogs/allBlogs')) active @endif"><a--}}
                            {{--                                            href="{{route('blogs.allblogs')}}"><i class="fa fa-circle-o"></i> All Blogs</a>--}}
                            {{--                                    </li>--}}
                            @endrole
                    </ul>
                </li>
                @endrole



                @hasrole('admin')
                <li class="@if(\Request::is('orders')) active @endif"><a href="{{route('orders.index')}}"><i
                            class="ion ion-bag"></i> All Orders
                             <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{count($orders)}}</small>
                                    </span>
                    </a>
                </li>
                @endrole


                @hasrole('admin')
                @else
                    <li class="@if(\Request::is('orders')) active @endif">
                        <a href="{{route('orders.index')}}"><i
                                class="fa fa-circle-o"></i> My Orders
                            <span class="pull-right-container">
                                        <small class="label pull-right bg-blue">{{count($orders)}}</small>
                                    </span>
                        </a>
                    </li>
                @endrole

{{--                <li class="treeview @if(\Request::is('orders')) active menu-open @endif">--}}
{{--                    <a href="#">--}}
{{--                        <i class="ion ion-bag"></i><span>Orders</span>--}}
{{--                        <span class="pull-right-container">--}}
{{--                  <i class="fa fa-angle-left pull-right"></i>--}}
{{--                </span>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        @hasrole('admin')--}}
{{--                        <li class="@if(\Request::is('orders')) active @endif"><a href="{{route('orders.index')}}"><i--}}
{{--                                    class="fa fa-circle-o"></i> All Orders--}}
{{--                                <span class="pull-right-container">--}}
{{--                                        <small class="label pull-right bg-blue">{{count($orders)}}</small>--}}
{{--                                    </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        @else--}}
{{--                            <li class="@if(\Request::is('orders')) active @endif">--}}
{{--                                <a href="{{route('orders.index')}}"><i--}}
{{--                                        class="fa fa-circle-o"></i> My Orders--}}
{{--                                    <span class="pull-right-container">--}}
{{--                                        <small class="label pull-right bg-blue">{{count($orders)}}</small>--}}
{{--                                    </span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @endrole--}}
{{--                    </ul>--}}
{{--                </li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
