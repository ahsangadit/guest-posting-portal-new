@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Industries
                <small>Add Industry</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('industry.index')}}">Industries</a></li>
                <li class="active">Add Industry</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="col-md-9">
                                {{ Form::open(array('url' => 'industry')) }}
                                {{--            @csrf--}}
                                <div class="form-group">
                                    {{ Form::label('keyword', 'Industry') }}
                                    {{ Form::text('industry', '', array('class' => 'form-control @error("industry") is-invalid @enderror')) }}

                                    @error('industry')
                                        <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>

                                {{ Form::submit('Add Industry', array('class' => 'btn bg-navy')) }}

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            // $('.textarea').wysihtml5();
        })
    </script>

@endsection
