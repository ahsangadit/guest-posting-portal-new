@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Permissions
                <small>All Permissions</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('permissions.index')}}" class="active">Permissions</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Permissions</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($permissions as $key => $permission)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{ $permission->name }}</td>
                                    <td>
                                     @hasrole('admin')
                              
                                        <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="btn btn-primary pull-left" style="margin-right: 3px;"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></a>

                                        {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
{{--                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                                        <button type="submit" class="btn btn-danger"> <i class="fa fa-trash"></i> </button>

                                        {!! Form::close() !!}

                                    @else
                                    <a href="{{ URL::to('permissions/'.$permission->id) }}" class="btn btn-success pull-left" style="margin-right: 3px;">Show</a>
                                    @endrole
                                    </td>                         
                                </tr>
                                    @endforeach
                                </tbody>
{{--                                <tfoot>--}}
{{--                                <tr>--}}
{{--                                    <th>S.No</th>--}}
{{--                                    <th>Permissions</th>--}}
{{--                                    <th>Operation</th>--}}
{{--                                </tr>--}}
{{--                                </tfoot>--}}
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
