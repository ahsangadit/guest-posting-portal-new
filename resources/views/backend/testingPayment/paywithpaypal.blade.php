@extends('layouts.backend.app')

@section('section')
    @php

        $user = Auth::user();

    @endphp
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->

        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-center">
                            <h1>Billing Info</h1>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-center">
                            <h2>   Welcome to Guest Posting Portal, In order to work with us you must
                                pay $1 dollar using PayPal.
                            </h2>

                            <p class="text-center" style="margin-top: 50px;">
                                <img src="{{asset('images/cc-badges-ppmcvdam.png')}}"
                                     alt="Buy now with PayPal"/>
                            </p>

                            <form class="form-horizontal" method="POST" id="payment-form" role="form"
                                  action="{!! URL::route('addmoney.paypal') !!}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }} text-center">

                                    <div class="col-md-12 text-center">
                                    </div>

                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">

                                        <input type="hidden" name="amount" value="1">
                                        <input type="hidden" id="credit_value" name="credit_value"
                                               value="{{$user->credit}}">
                                        {{-- <input id="amount" type="text" class="form-control " name="amount" value="{{ old('amount') }}" autofocus>--}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('amount') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn bg-navy-active btn-lg custom-img-paypal">
                                            Pay Now
                                        </button>
                                        {{-- <div class="myButton"><input type="submit" name="" value=""></div>--}}
                                    </div>
                                </div>

                            </form>


                        </div>
                        <div class="col-md-2"></div>

                    </div>
                </div>
            </div>


            <!-- Button trigger modal -->
        {{--            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modelId">--}}
        {{--                Launch--}}

        {{--            </button>--}}


        <!-- Modal -->
            <div class="modal fade" id="PaymentModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            Pay with Paypal
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form class="form-horizontal" method="POST" id="payment-form" role="form"
                                  action="{!! URL::route('addmoney.paypal') !!}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }} text-center">

                                    <div class="col-md-12 text-center">
                                        <h3>Billing Info</h3>
                                    </div>

                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <label for="amount" class="text-center">
                                            Welcome to Guest Posting Portal, In order to work with us you must
                                            pay $1 dollar using PayPal.
                                        </label>

                                        <input type="hidden" name="amount" value="1">
                                        <input type="hidden" id="credit_value" name="credit_value"
                                               value="{{$user->credit}}">
                                        {{-- <input id="amount" type="text" class="form-control " name="amount" value="{{ old('amount') }}" autofocus>--}}
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('amount') }}</strong>
                                                    </span>
                                        @endif

                                        <img src="{{asset('images/cc-badges-ppmcvdam.png')}}"
                                             alt="Buy now with PayPal"/>
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn bg-navy-active custom-img-paypal">
                                            Pay Now
                                        </button>
                                        {{-- <div class="myButton"><input type="submit" name="" value=""></div>--}}
                                    </div>
                                </div>

                            </form>


                            {{--                            <div class="panel panel-default">--}}
                            {{--                                <div class="panel-heading text-center">Pay with Paypal</div>--}}
                            {{--                                <div class="panel-body">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {


        });
    </script>
@endsection

{{--@extends('layouts.backend.app')--}}

{{--@section('section')--}}

{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-8 col-md-offset-2">--}}
{{--                <div class="panel panel-default">--}}
{{--                    <div class="panel-heading">Paywith Paypal</div>--}}
{{--                    <div class="panel-body">--}}
{{--                        <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{!! URL::route('addmoney.paypal') !!}" >--}}
{{--                            {{ csrf_field() }}--}}
{{--                            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">--}}
{{--                                <label for="amount" class="col-md-4 control-label">Amount</label>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" autofocus>--}}
{{--                                    @if ($errors->has('amount'))--}}
{{--                                        <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('amount') }}</strong>--}}
{{--                                    </span>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <div class="col-md-6 col-md-offset-4">--}}
{{--                                    <button type="submit" class="btn btn-primary">--}}
{{--                                        Paywith Paypal--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--@endsection--}}
