@extends('layouts.backend.app')

@section('section')

    <div class="content-wrapper">

        @include('layouts.backend.alert')

        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-cog"></i>
                            <h3 class="box-title">Setting</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <dl>
                                <dt class="custom-dt">
                                    <h4><u>Account:</u>
                                    </h4>
                                </dt>
                                <dd class="custom-dd"><a href="{{route('user.setting')}}" class="custom-dd-a">
                                        Profile </a></dd>
                                <dd class="custom-dd"><a href="{{route('user.changePassword')}}" class="custom-dd-a">
                                        Change Password </a></dd>
                            </dl>


                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-8">

                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-user"></i>
                            <h3 class="box-title">Profile</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body text-center">
                            <form action="" method="post">

                                <div class="row">
                                    <div class="col-md-3" style="display: table;height: 100px;">
                                        <div class="custom-vertical-align">
                                            <div class="form-group">
                                                <label for="">Avatar:</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <img src="{{ asset('backend/img/user2-160x160.jpg')  }}" alt="" width="80"
                                                 height="80">
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="display: table;height: 100px;">
                                        <div class="custom-vertical-align">
                                            <input type="file" name="" id="">
                                        </div>
                                    </div>
                                </div>
{{--                                <label for="">ValueIs</label>--}}
{{--                                <input type="checkbox" name="valueis" id="valueis">--}}
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {

            $('#valueis').change(function () {
                if ($(this).prop('checked')) {


                    $.ajax({
                        type: "GET",
                        url: '<?php route('users.testing');?>',
                        data: {test: "checked"},
                        success: function (response) {
                           console.log(response)
                        },
                        error: function (response) {
                            alert('Error-' + response);
                        }
                    });


                }

            });

        });
    </script>
@endsection
