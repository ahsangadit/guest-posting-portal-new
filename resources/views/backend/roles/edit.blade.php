@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Roles
                <small>Edit Role</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('roles.index')}}" class="active">Roles</a></li>
                <li class="active">Edit Role</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">

                            <div class="col-md-6">
                                {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

                                <div class="form-group">
                                    {{ Form::label('name', 'Role Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control @error("name") is-invalid @enderror')) }}

                                    @error('name')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>

                                <h5><b>Assign Permissions</b></h5>
                                @foreach ($permissions as $permission)

                                    {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                                    {{Form::label($permission->name, ucfirst($permission->name)) }}<br>
                                @endforeach
                                @error('permissions')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <br>
                                {{ Form::submit('Update Role', array('class' => 'btn bg-navy')) }}

                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection
