@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Order Detail
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Order</a></li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <form role="form">
                                <h4> User Detail </h4>
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="box-body">
                                                <dl class="dl-horizontal" style="text-align: left;">
                                                    <dt>First Name:</dt>
                                                    <dd>{{$user_detail[0]['fname']}}</dd>
                                                    <br>
                                                    <dt>Last Name:</dt>
                                                    <dd>{{$user_detail[0]['lname']}}</dd>
                                                    <br>
                                                    <dt>Phone No:</dt>
                                                    <dd>{{$user_detail[0]['phone_number']}}</dd>
                                                    <br>
                                                    <dt>Country:</dt>
                                                    <dd>{{$user_detail[0]['country']}}</dd>
                                                    <br>
                                                </dl>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="box-body">
                                                <dl class="dl-horizontal" style="text-align: left;">
                                                    <dt>City:</dt>
                                                    <dd>{{$user_detail[0]['city']}}</dd>
                                                    <br>
                                                    <dt>Postal Code:</dt>
                                                    <dd>{{$user_detail[0]['post_code']}}</dd>
                                                    <br>
                                                    <dt>Phone No:</dt>
                                                    <dd>{{$user_detail[0]['phone_number']}}</dd>
                                                    <br>
                                                    <dt>Address:</dt>
                                                    <dd>{{$user_detail[0]['address']}}</dd>
                                                </dl>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <h4>Additional Details </h4>
                                <div class="box-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="box-body">
                                                <dl class="dl-horizontal" style="text-align: left;">
                                                    <dt>Web Site URL:</dt>
                                                    <dd> @if(!empty($user_detail[0]['website_url'])) {{$user_detail[0]['website_url']}} @endif  </dd>
                                                    <br>
                                                    <dt>Anchor:</dt>
                                                    <dd> @if(!empty($user_detail[0]['anchor'])) {{$user_detail[0]['anchor']}} @endif </dd>
                                                    <br>
                                                    <dt>Word Count:</dt>
                                                    <dd>@if(!empty($user_detail[0]['wordcount'])) {{$user_detail[0]['wordcount']}} @endif</dd>
                                                    <br>
                                                    <dt>Images:</dt>
                                                    <dd>@if(!empty($user_detail[0]['image_count'])) {{$user_detail[0]['image_count']}} @endif</dd>

                                                </dl>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!-- /.box-body -->

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">

                            <h4> Order Detail </h4>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Blogs</th>
                                    <th>Additional Details</th>
                                    <th>Price</th>
                                    <th class="text-center">Subtotal</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>


                                @foreach($order as $key => $val)

                                    <tr style="background:white;">
                                        <td data-th="Product">
                                            <div class="row">
                                                <div class="col-sm-3 hidden-xs">

                                                    @if($val['blog_image'])
                                                        <img src="{{asset('storage/'.$val['blog_image'])}}" alt="..."
                                                             class="img-responsive"/></div>
                                                @else
                                                    <img src="http://placehold.it/100x100" alt="..."
                                                         class="img-responsive"/>
                                                @endif

                                                <div class="col-sm-9">
                                                    <h4 class="nomargin">{{ $val['title']  }}</h4>
                                                    <p> {{ substr(strip_tags($val['description']), 0 , 50)."..." }} </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td data-th="Price">
                                            DA: {{ $val['da'] }}<br>
                                            FB: {{ $val['fb'] }} <br>
                                            Followers: {{ $val['follower'] }}
                                        </td>
                                        <td data-th="Price">
                                            {{ "$".number_format($val['price'],0) }}
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="background:white;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if($key+1 == $total_blogs )
                                            <td class="text-center"><strong>{{ "$".number_format($total,0) }}</strong>
                                            </td>
                                            <td><strong> {{ ucfirst($order_status) }} </strong></td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>

                                @endforeach


                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection
