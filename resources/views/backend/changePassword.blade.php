@extends('layouts.backend.app')

@section('section')

    <div class="content-wrapper">

        @include('layouts.backend.alert')

        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-cog"></i>
                            <h3 class="box-title">Setting</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <dl>
                                <dt class="custom-dt">
                                    <h4>
                                        <u>Account:</u>
                                    </h4>
                                </dt>
                                <dd class="custom-dd"><a href="{{route('user.setting')}}" class="custom-dd-a">
                                        Profile </a></dd>
                                <dd class="custom-dd"><a href="{{route('user.changePassword')}}" class="custom-dd-a">
                                        Change Password </a></dd>
                            </dl>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-8">

                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-key"></i>
                            <h3 class="box-title">Change Password</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <dl>
                                <dt class="custom-dt">
                                     <dd class="custom-dd-normal">
                                          If you need to change your password, please do so below.
                                     </dd>
                                </dt>

                                <div class="change-password-sec">
                                    <form method="post" action="{{route('users.updatePassword' , Auth::user()->id)}}">
                                        @csrf
                                        <div class="form-group">
                                            <label for="">Password:</label>
                                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>

                                        <div class="form-group">
                                            <label for="">Confirm Password:</label>
                                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">

                                            @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert" style="color: red;">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <input type="submit" value="Update Password" class="btn bg-navy btn-flat">
                                        </div>

                                    </form>
                                </div>

                            </dl>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script>

    </script>
@endsection
