@extends('layouts.backend.app')

@section('section')

    <div class="content-wrapper">

    @include('layouts.backend.alert')

    @php
        $user_ids = [];
        $blogs_id = [];
        $logs_details = [];
        $blogs="";

        $logs = \App\order_log::where(['user_id' => Auth::user()->id])->get();

        foreach ($logs as $key => $arr){

           $previous_logs = unserialize($arr->meta_value);

           foreach ($previous_logs as $i => $arr_val){
                $blogs_id[] = $arr_val['blog_id'];
           }

        }

    @endphp

    <!-- add to cart alert message section -->
        <section class="content-header">
            <div class="row">
                <div class="col-md-12">

                    <div class="custom-alerts alert alert-success add-to-cart-message"><em> Blog Added to Cart </em>
                    </div>

                    {{--                    <div class="alert alert-success alert-dismissible add-to-cart-message">--}}
                    {{--                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>--}}
                    {{--                        <h4><i class="icon fa fa-check"></i> Cart Updated!</h4>--}}
                    {{--                        Blog Added to Cart--}}
                    {{--                    </div>--}}

                </div>
            </div>
        </section>


        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blogs
                <small>All Blog</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('blogs.index')}}">Blogs</a></li>
            </ol>

        </section>


        <section class="content min-height-0">

            <div class="row">
                <div class="col-md-12">
                    <form method="post" enctype="multipart/form-data" action="{{route('blogs.searchresult')}}">
                        {{--                    <div class="col-md-6">--}}
                        {{--                        <div class="input-group margin">--}}
                        {{--                            <input type="hidden" value="{{csrf_token()}}" name="_token"/>--}}
                        {{--                            <input class="form-control input-lg" name="keyword" type="text"--}}
                        {{--                                   placeholder="Search by keyword">--}}

                        {{--                            <span class="input-group-btn">--}}
                        {{--                                 <button type="submit" class="btn btn-block bg-navy btn-lg">Search</button>--}}
                        {{--                             </span>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}

                        <div class="box box-primary">
                            <div class="box-header with-border">

                                <div class="col-md-8">
                                    <div class="box-body">
                                        <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                                        <input class="form-control input-lg" name="keyword" value="{{old('keyword' , $fields_value['keyword'])}}" type="text"
                                               placeholder="Search by keyword, URL">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="box-body">
                                        <button type="submit" class="btn btn-block bg-navy btn-lg">Search</button>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Domain Authority Filter</label>
                                            <select class="form-control" name="da">
                                                <option value="0-0">Select Range</option>
                                                <option value="10-20" @if($fields_value['da'] == '10-20') selected @endif>10 - 20</option>
                                                <option value="20-30" @if($fields_value['da'] == '20-30') selected @endif>20 - 30</option>
                                                <option value="30-40" @if($fields_value['da'] == '30-40') selected @endif>30 - 40</option>
                                                <option value="40-50" @if($fields_value['da'] == '40-50') selected @endif>40 - 50</option>
                                                <option value="50-60" @if($fields_value['da'] == '50-60') selected @endif>50 - 60</option>
                                                <option value="60-70" @if($fields_value['da'] == '60-70') selected @endif>60 - 70</option>
                                                <option value="70-80" @if($fields_value['da'] == '70-80') selected @endif>70 - 80</option>
                                                <option value="80-90" @if($fields_value['da'] == '80-90') selected @endif>80 - 90</option>
                                                <option value="90-100" @if($fields_value['da'] == '90-100') selected @endif>90 - 100</option>
                                                {{--<option value="">100+</option>--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Page Authority Filter</label>
                                            <select class="form-control" name="fb_likes">
                                                <option value="0-0">Select Range</option>
                                                <option value="01-10" @if($fields_value['fb_likes'] == '01-10') selected @endif>01 - 10</option>
                                                <option value="10-20" @if($fields_value['fb_likes'] == '10-20') selected @endif>10 - 20</option>
                                                <option value="20-30" @if($fields_value['fb_likes'] == '20-30') selected @endif>20 - 30</option>
                                                <option value="30-40" @if($fields_value['fb_likes'] == '30-40') selected @endif>30 - 40</option>
                                                <option value="40-50" @if($fields_value['fb_likes'] == '40-50') selected @endif>40 - 50</option>
                                                <option value="50-60" @if($fields_value['fb_likes'] == '50-60') selected @endif>50 - 60</option>
                                                <option value="60-70" @if($fields_value['fb_likes'] == '60-70') selected @endif>60 - 70</option>
                                                <option value="70-80" @if($fields_value['fb_likes'] == '70-80') selected @endif>70 - 80</option>
                                                <option value="80-90" @if($fields_value['fb_likes'] == '80-90') selected @endif>80 - 90</option>
                                                <option value="90-100" @if($fields_value['fb_likes'] == '90-100') selected @endif>90 - 100</option>
                                                {{--                                                <option value="100+">100+</option>--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Ahrefs Organic Traffic Range filter</label>
                                            <select class="form-control" name="follower">
                                                <option value="0-0"  @if($fields_value['follower'] == '1-50') selected @endif>Select Range</option>
                                                <option value="1-500"  @if($fields_value['follower'] == '1-500') selected @endif>1 - 500</option>
                                                <option value="500-1000"  @if($fields_value['follower'] == '500-1000') selected @endif>500 - 1k</option>
                                                <option value="1000-5000"  @if($fields_value['follower'] == '1000-5000') selected @endif>1k - 5k</option>
                                                <option value="5000-15000"  @if($fields_value['follower'] == '5000-15000') selected @endif>5k - 15k</option>
                                                <option value="30000-50000"  @if($fields_value['follower'] == '30000-50000') selected @endif>30k - 50k</option>
                                                <option value="50000-100000"  @if($fields_value['follower'] == '50000-100000') selected @endif>50k - 100k</option>
                                                <option value="100000-500000"  @if($fields_value['follower'] == '100000-500000') selected @endif>100k - 500k</option>
                                                {{--                                                <option value="500000">500k+</option>--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Price Range Filter</label>
                                            <select class="form-control" name="price">
                                                <option value="0-0">Select Range</option>
                                                <option value="1-50" @if($fields_value['price'] == '1-50') selected @endif>$1 - $50</option>
                                                <option value="50-100" @if($fields_value['price'] == '50-100') selected @endif>$50 - $100</option>
                                                <option value="200-300" @if($fields_value['price'] == '200-300') selected @endif>$200 - $300</option>
                                                <option value="300-400" @if($fields_value['price'] == '300-400') selected @endif>$300 - $400</option>
                                                <option value="400-500" @if($fields_value['price'] == '400-500') selected @endif>$400 - $500</option>
                                                <option value="500-600" @if($fields_value['price'] == '500-600') selected @endif>$500 - $600</option>
                                                <option value="600-700" @if($fields_value['price'] == '600-700') selected @endif>$600 - $700</option>
                                                <option value="700-800" @if($fields_value['price'] == '700-800') selected @endif>$700 - $800</option>
                                                <option value="800-900" @if($fields_value['price'] == '800-900') selected @endif>$800 - $900</option>
                                                <option value="900-1000" @if($fields_value['price'] == '900-1000') selected @endif>$900 - $1k</option>
                                                {{--                                                <option value="1000">$1k+</option>--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </form>
                </div>
            </div>

        </section>




    @isset($blog)

            <section class="content">

                <div class="row">

                    @forelse($blog as $key => $blog)

                        <?php
                            //echo $blog->blog_meta[2]->meta_value;
                        ?>

                        <form method="post" class="add_to_cart_form" enctype="multipart/form-data"
                              action="{{route('blogs.add_to_cart')}}">
                            <div class="col-md-4">
                                <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                <!--style="background: url('{{asset('storage/'.$blog->blog_image)}}') center center;height: 200px;background-size: cover;" Add the bg color to the header custom-bg-blog using any of the bg-* classes -->

                                    <div class="box-overlay">
                                        <div class="widget-user-header blog-box"
                                             @if($blog->blog_image) style="background: url('{{asset('storage/'.$blog->blog_image)}}') center center;height: 200px;background-size: cover;"
                                             @else style="background:url('{{asset('images/no-image-available.png')}}') ;height: 200px;background-size: cover;" @endif>

                                            {{--                                            @if($blog->blog_image)--}}
                                            {{--                                                <img src="{{asset('storage/'.$blog->blog_image)}}" alt="blogImage" class="blog-img">--}}
                                            {{--                                            @else--}}
                                            {{--                                                <div>--}}
                                            {{--                                                    <img src="{{asset('images/no-image-available.png')}}" alt="placeHolder"--}}
                                            {{--                                                         class="blog-img">--}}
                                            {{--                                                </div>--}}
                                            {{--                                                --}}{{--<div style="background-color: firebrick;height: 50vh;width: 100%;"></div>--}}
                                            {{--                                            @endif--}}


                                            <h3 class="widget-user-username"
                                                style="color: white;text-shadow: 0px 0px 5px black;"><a
                                                    class="blog-href"
                                                    href="{{route('blogs.viewDetails',$blog->id)}}">{{ ucfirst($blog->title) }}</a>
                                            </h3>

                                            <h5 class="widget-user-desc"
                                                style="color: white;text-shadow: 0px 0px 5px black;">
                                                {{strip_tags(substr($blog->description ,0 , 120))."..." }}
                                            </h5>
                                        </div>
                                    </div>

                                    <!-- <div class="widget-user-image">
                                      <img class="img-circle" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                                    </div> -->
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{$blog->blog_meta[0]->meta_value}}</h5>
                                                    <span class="description-text">DA</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{$blog->blog_meta[2]->meta_value}}</h5>
                                                    <span class="description-text">A-hrefs</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{$blog->blog_meta[1]->meta_value}}</h5>
                                                    <span class="description-text">PA</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-md-4 col-sm-6">
                                                <div class="description-block hidden-fields">

                                                    <h5 class="description-header">
                                                        ${{number_format($blog->price,0)}}</h5>
                                                    <span class="description-text">Price</span>

                                                    <input type="hidden" class="cart_post_web_url" name="url"
                                                           value="{{url('/blogs/add_to_cart')}}"/>
                                                    <input type="hidden" name="title" class="cart_title"
                                                           value="{{ $blog->title}}"/>
                                                    <input type="hidden" class="cart_web_url" name="url"
                                                           value="{{ $blog->link }}"/>
                                                    <input type="hidden" name="description" class="cart_description"
                                                           value="{{ $blog->description}}"/>
                                                    <input type="hidden" name="price" class="cart_price"
                                                           value="{{ $blog->price}}"/>
                                                    <input type="hidden" name="da" class="cart_da"
                                                           value="{{$blog->blog_meta[0]->meta_value}}"/>
                                                    <input type="hidden" name="fb" class="cart_fb"
                                                           value="{{$blog->blog_meta[1]->meta_value}}"/>
                                                    <input type="hidden" name="follower" class="cart_follower"
                                                           value="{{$blog->blog_meta[2]->meta_value}}"/>
                                                    <input type="hidden" name="user_id" class="cart_user_id"
                                                           value="{{Auth::user()->id }}"/>
                                                    <input type="hidden" name="blog_id" class="cart_blog_id"
                                                           value="{{$blog->id}}"/>
                                                    <input type="hidden" name="blog_image" class="cart_blog_image"
                                                           value="{{$blog->blog_image}}"/>
                                                <!-- <h5 class="description-header">{{$blog->price}}</h5> -->
                                                    {{--                                                    <span class="description-text">--}}
                                                    {{--                                                         <button class="btn btn-block btn-success addtocart" type="submit" name="add-to-cart">Starting At $ {{$blog->blog->price}}</button>--}}
                                                    {{--                                                    </span>--}}
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <div class="col-md-2 col-lg-4"></div>
                                            <div class="col-md-4 col-sm-6">
                                                <div class="description-block ">

                                            <span class="description-text addToCart-button-section">

                                                 @if(in_array($blog->id ,array_unique($blogs_id) ))
                                                     <input type="hidden" exists_blog_id="{{$blog->id}}"
                                                            class="cart_exists_blog_id">
                                                 @else
                                                     <input type="hidden" exists_blog_id="null"
                                                            class="cart_exists_blog_id">
                                                 @endif


                                                  <label for=""
                                                         class="label label-success label-lg addedLabel">Added</label>

                                                  <button class="btn bg-navy addtocart" type="submit"
                                                          name="add-to-cart">Add to cart</button>

{{--                                                  @if(in_array($blog->id ,array_unique($blogs_id) ))--}}
{{--                                                      <label for="" class="label label-success label-lg addedLabel">Added</label>--}}
{{--                                                  @else--}}
{{--                                                      <button class="btn bg-navy addtocart" type="submit"--}}
{{--                                                              name="add-to-cart">Add to cart</button>--}}
{{--                                                  @endif--}}
                                            </span>


                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>
                        </form>
                    @empty
                        <h2 class='text-center'>No Blog Found</h2>
                @endforelse
                <!-- /.col -->
                </div>
            </section>
    @endisset
    <!-- Main content -->

    <!-- After Search Result -->
    @isset($result)
        <!-- dd($result); -->
            <section class="content">

                <div class="row">

                    @forelse($result as $key => $id)
                        @php
                            $blog = \App\blog::find($id);
                           // $blog_metas = \App\blog_meta::where(['blog_id' => $blogs->id])->where(['meta_key' => 'da'])->get();
                        @endphp

                        <form method="post" class="add_to_cart_form" enctype="multipart/form-data"
                              action="{{route('blogs.add_to_cart')}}">
                            <div class="col-md-4">
                                <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                <!--style="background: url('{{asset('storage/'.$blog->blog_image)}}') center center;height: 200px;background-size: cover;" Add the bg color to the header custom-bg-blog using any of the bg-* classes -->

                                    <div class="widget-user-header blog-box" @if($blog->blog_image) style="background: url('{{asset('storage/'.$blog->blog_image)}}') center center;height: 200px;background-size: cover;"
                                         @else style="background:url('{{asset('images/no-image-available.png')}}') ;height: 200px;background-size: cover;" @endif>


                                        <h3 class="widget-user-username"
                                            style="color: white;text-shadow: 0px 0px 5px black;"><a
                                                class="blog-href"
                                                href="{{route('blogs.viewDetails',$blog->id)}}">{{ ucfirst($blog->title) }}</a>
                                        </h3>


                                        <h5 class="widget-user-desc"
                                            style="color: white;text-shadow: 0px 0px 5px black;">
                                            {{strip_tags(substr($blog->description ,0 , 120))."..." }}
                                        </h5>
                                    </div>
                                    <!-- <div class="widget-user-image">
                                      <img class="img-circle" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                                    </div> -->
                                    <div class="box-footer">
                                        <div class="row">
                                        <div class="col-sm-4 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header">{{$blog->blog_meta[0]->meta_value}}</h5>
                                                <span class="description-text">DA</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header">{{$blog->blog_meta[2]->meta_value}}</h5>
                                                <span class="description-text">A-hrefs</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4">
                                            <div class="description-block">
                                                <h5 class="description-header">{{$blog->blog_meta[1]->meta_value}}</h5>
                                                <span class="description-text">PA</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        </div>
                                        <div class="row">
                                            <!-- /.col -->
                                            <div class="col-md-4 col-sm-6">
                                                <div class="description-block hidden-fields">

                                                    <h5 class="description-header">
                                                        ${{number_format($blog->price,0)}}</h5>
                                                    <span class="description-text">Price</span>

                                                    <input type="hidden" name="title" class="cart_title"
                                                           value="{{ $blog->title}}"/>
                                                    <input type="hidden" class="cart_post_web_url" name="url"
                                                           value="{{url('/blogs/add_to_cart')}}"/>
                                                    <input type="hidden" class="cart_web_url" name="url"
                                                           value="{{ $blog->link }}"/>
                                                    <input type="hidden" name="description" class="cart_description"
                                                           value="{{ $blog->description}}"/>
                                                    <input type="hidden" name="price" class="cart_price"
                                                           value="{{ $blog->price}}"/>
                                                    <input type="hidden" name="da" class="cart_da"
                                                           value="{{$blog->blog_meta[0]->meta_value}}"/>
                                                    <input type="hidden" name="fb" class="cart_fb"
                                                           value="{{$blog->blog_meta[1]->meta_value}}"/>
                                                    <input type="hidden" name="follower" class="cart_follower"
                                                           value="{{$blog->blog_meta[2]->meta_value}}"/>
                                                    <input type="hidden" name="user_id" class="cart_user_id"
                                                           value="{{Auth::user()->id }}"/>
                                                    <input type="hidden" name="blog_id" class="cart_blog_id"
                                                           value="{{$blog->id}}"/>
                                                    <input type="hidden" name="blog_image" class="cart_blog_image"
                                                           value="{{$blog->blog_image}}"/>
                                                <!-- <h5 class="description-header">{{$blog->price}}</h5> -->

                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <div class="col-md-2 col-lg-4"></div>
                                            <div class="col-md-4 col-sm-6">
                                                <div class="description-block ">
                                                     <span class="description-text addToCart-button-section">

                                                         @if(in_array($blog->id ,array_unique($blogs_id) ))
                                                             <input type="hidden" exists_blog_id="{{$blog->id}}"
                                                                    class="cart_exists_blog_id">
                                                         @else
                                                             <input type="hidden" exists_blog_id="null"
                                                                    class="cart_exists_blog_id">
                                                         @endif


                                                         <label for="" class="label label-success label-lg addedLabel">Added</label>
                                                         <button class="btn bg-navy addtocart" type="submit"
                                                                 name="add-to-cart">Add to cart</button>

{{--                                                         @if(in_array($blog->id ,array_unique($blogs_id) ))--}}
{{--                                                             <label for="" class="label label-success label-lg addedLabel">Added</label>--}}
{{--                                                             <label for="" class="label label-success addedLabel">Added</label>--}}
{{--                                                         @else--}}
{{--                                                           <button class="btn btn-success addtocart" type="submit"--}}
{{--                                                                    name="add-to-cart">Add to cart</button>--}}
{{--                                                               <button class="btn bg-navy addtocart" type="submit"--}}
{{--                                                                    name="add-to-cart">Add to cart</button>--}}
{{--                                                         @endif--}}

                                                     </span>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>
                        </form>

                    @empty
                        <h2 class='text-center'>No Blog Found</h2>
                    @endforelse
                    {{--                <!-- /.col -->--}}
                </div>
            </section>

    @endisset
    <!-- /.content -->
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection
