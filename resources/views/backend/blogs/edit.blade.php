@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blogs
                <small>Edit blog</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('blogs.index')}}">Blogs</a></li>
                <li class="active">Edit Blog</li>
            </ol>
        </section>


        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">

                            {{ Form::model($blog, array('route' => array('blogs.update', $blog->id), 'method' => 'POST', 'files' => true)) }}

                            {{-- <form method="post" action="{{ route('blogs.update',['id'=>$id]) }}" enctype='multipart/form-data' accept-charset="UTF-8"> --}}
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ Form::label('link', 'Blog Link') }}
                                    {{ Form::text('link', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Blog Title') }}
                                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Price') }}
                                    {{ Form::number('price', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Domain Authority') }}
                                    {{ Form::number('da', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'PA') }}
                                    {{ Form::number('fb_likes', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Ahrefs Organic Traffic') }}
                                    {{ Form::number('follower', null, array('class' => 'form-control')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Industries') }}
                                    {{ Form::select('industries',$industries,$blog['industry'],array('multiple'=>'multiple','name'=>'industry[]','class' => 'form-control select2','data-placeholder'=>'Select a Industry'))}}
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    {{ Form::label('name', 'Keywords') }}
                                    {{ Form::select('keywords',$keywords,$blog['keyword'],array('multiple'=>'multiple','name'=>'keyword[]','class' => 'form-control select2','data-placeholder'=>'Select a keyword'))}}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Description') }}
                                    {{ Form::textarea('description', null, array('class' => 'form-control textarea')) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::label('name', 'Upload Image') }}
                                    {{ Form::file('image' , array('class' => 'form-control @error("image") is-invalid @enderror'))}}

                                    <p class="help-block">Upload blog picture here.</p>

                                    @error('image')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                    @enderror

                                </div>

                            </div>


                            <div class="col-md-6">

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('name', 'Dropped') }}
                                            {{ Form::radio('dropped', 'true' , true,array('class' => 'minimal-red')) }}
                                            {{ Form::radio('dropped', 'false' , true,array('class' => 'minimal-red')) }}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('name', 'flag') }}
                                            {{ Form::radio('flag', 'true' , true,array('class' => 'minimal-red')) }}
                                            {{ Form::radio('flag', 'false' , true,array('class' => 'minimal-red')) }}
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::submit('Update Blog', array('class' => 'btn bg-navy')) }}
                                        </div>
                                    </div>

                                </div>

                            </div>

                            {{ Form::close() }}
                            {{-- </form> --}}

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
            $('.select2').select2();
        })

    </script>
@endsection
