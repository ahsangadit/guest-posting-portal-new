@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

    @php

        $user_ids = [];
        $blogs_id = [];
        $logs_details = [];
        $blogs="";

        $logs = \App\order_log::where(['user_id' => Auth::user()->id])->get();

        foreach ($logs as $key => $arr){

           $previous_logs = unserialize($arr->meta_value);

           foreach ($previous_logs as $i => $arr_val){
                $blogs_id[] = $arr_val['blog_id'];
           }

        }

    @endphp

    <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blogs
                <small>Show blog</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('blogs.index')}}">Blogs</a></li>
                <li>{{ucfirst($blog->title)}}</li>
                <li class="active">View Details</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">

                            <div class="col-md-12">
                                <h2> {{ucfirst($blog->title)}}</h2>
                            </div>

                            <div class="col-md-6">
                                @if($blog->blog_image)
                                    <img src="{{asset('storage/'.$blog->blog_image)}}" alt="blogImage" class="blog-img">
                                @else
                                    <div>
                                        <img src="{{asset('images/no-image-available.png')}}" alt="placeHolder"
                                             class="blog-img">
                                    </div>
                                    {{--<div style="background-color: firebrick;height: 50vh;width: 100%;"></div>--}}
                                @endif
                            </div>
                            <div class="col-md-6">

                                <h3>Descriptions:</h3>
                                <p>
                                    {{strip_tags($blog->description)}}
                                </p>

                                <h4>Price:</h4>
                                <p>
                                    ${{number_format($blog->price,0)}}
                                </p>

                                <form method="post" class="add_to_cart_form" enctype="multipart/form-data"
                                      action="{{route('blogs.add_to_cart')}}">

                                    <input type="hidden" class="cart_post_web_url" name="url"
                                           value="{{url('/blogs/add_to_cart')}}"/>
                                    <input type="hidden" name="title" class="cart_title"
                                           value="{{ $blog->title}}"/>
                                    <input type="hidden" class="cart_web_url" name="url"
                                           value="{{ $blog->link }}"/>
                                    <input type="hidden" name="description" class="cart_description"
                                           value="{{ $blog->description}}"/>
                                    <input type="hidden" name="price" class="cart_price"
                                           value="{{ $blog->price}}"/>
                                    <input type="hidden" name="da" class="cart_da"
                                           value="{{$blog->blog_meta[0]->meta_value}}"/>
                                    <input type="hidden" name="fb" class="cart_fb"
                                           value="{{$blog->blog_meta[1]->meta_value}}"/>
                                    <input type="hidden" name="follower" class="cart_follower"
                                           value="{{$blog->blog_meta[2]->meta_value}}"/>
                                    <input type="hidden" name="user_id" class="cart_user_id"
                                           value="{{Auth::user()->id }}"/>
                                    <input type="hidden" name="blog_id" class="cart_blog_id"
                                           value="{{$blog->id}}"/>
                                    <input type="hidden" name="blog_image" class="cart_blog_image"
                                           value="{{$blog->blog_image}}"/>

                                    <span class="description-text addToCart-button-section">

                                        @if(in_array($blog->id ,array_unique($blogs_id) ))
                                            <input type="hidden" exists_blog_id="{{$blog->id}}"
                                                   class="cart_exists_blog_id">
                                        @else
                                            <input type="hidden" exists_blog_id="null"
                                                   class="cart_exists_blog_id">
                                        @endif

                                            <label for="" class="label label-success label-lg addedLabel">Added</label>
                                            <button class="btn bg-navy addtocart" type="submit" name="add-to-cart">Add to cart</button>

                                        </span>

                                </form>
                            </div>


                            <div class="col-md-12"><br></div>

                        </div>
                    </div>


                    <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                        <div class="nav-tabs-custom">
                            <ul class="pull-right nav nav-tabs">

                                {{--                                <li class=""><a href="#tab_3-2" data-toggle="tab" aria-expanded="false">Tab 3</a></li>--}}
                                {{--                                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Tab 2</a></li>--}}
                                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Site
                                        Details</a></li>

                                <li class="pull-left header"><i class="fa fa-th"></i> Blog Details</li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1-1">

                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Website URL:</th>
                                            <td><a href="{{ $blog->link }}">{{ $blog->link }}</a></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Keywords</th>
                                            <td>
                                                @foreach ($blog->keywords as $key)
                                                    <label href="#" class="label label-success viewDetail-keywords">{{$key->keyword}}</label>
                                                @endforeach
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Industry</th>
                                            <td>
                                                @foreach ($blog->industries as $ind)
                                                    <label class="label label-success viewDetail-industries">{{$ind->industry}}</label>
                                                @endforeach
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Domain Authority</th>
                                            <td>{{ $blog_metas['da'] }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Page Authority</th>
                                            <td> {{$blog_metas['fb_likes']}} </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <th>Ahrefs Organic Traffic</th>
                                            <td>{{ $blog_metas['follower'] }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>

                                <!-- /.tab-pane -->
                            {{--                                <div class="tab-pane" id="tab_2-2">--}}
                            {{--                                    The European languages are members of the same family. Their separate existence is a myth.--}}
                            {{--                                    For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ--}}
                            {{--                                    in their grammar, their pronunciation and their most common words. Everyone realizes why a--}}
                            {{--                                    new common language would be desirable: one could refuse to pay expensive translators. To--}}
                            {{--                                    achieve this, it would be necessary to have uniform grammar, pronunciation and more common--}}
                            {{--                                    words. If several languages coalesce, the grammar of the resulting language is more simple--}}
                            {{--                                    and regular than that of the individual languages.--}}
                            {{--                                </div>--}}
                            <!-- /.tab-pane -->
                            {{--                                <div class="tab-pane" id="tab_3-2">--}}
                            {{--                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.--}}
                            {{--                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,--}}
                            {{--                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.--}}
                            {{--                                    It has survived not only five centuries, but also the leap into electronic typesetting,--}}
                            {{--                                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset--}}
                            {{--                                    sheets containing Lorem Ipsum passages, and more recently with desktop publishing software--}}
                            {{--                                    like Aldus PageMaker including versions of Lorem Ipsum.--}}
                            {{--                                </div>--}}
                            <!-- /.tab-pane -->

                            </div>
                            <!-- /.tab-content -->
                        </div>
                    </div>


                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
            $('.select2').select2();
        })

    </script>
@endsection
