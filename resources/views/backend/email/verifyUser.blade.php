@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])

    <img src="{{asset('backend/img/hztech.jpg')}}" alt="" class="light-logo" style="width: 100px;
    height: 80px;
    margin: 0 auto;
    margin-bottom: 20px;" />
    @endcomponent
  @endslot 
   <h1 style="
    font-size: 13px;
    font-weight: 600;
    margin-top: 0;
    text-align: left;
    color: #000;
">

<h2>Welcome to the site {{$user['name']}}</h2>
</h1>
<h2 style="
color: #ffffff;
background: #FF9800;
padding: 8px;
text-align: center;
"><a href="{{url('user/verify', $user->verifyUser->token)}}">Verify Email</a></h2>

@slot('subcopy')
    @component('mail::subcopy')
   
   @endcomponent
@endslot


{{-- Footer --}}

@slot('footer')
    @component('mail::footer')
        <table >
            <tr class="social-links">
                 <td class="left" style="float:right;margin-left:-10px;"><a href="#"><img src="{{asset('backend/img/fb.png')}}"/></a></td>
                <td><!--<img src="{{Url('storage/social-icon/twitter.png')}}"/>--></td>
               <td><a href="#"><img src="{{asset('backend/img/instagram.png')}}"/></a></td>
            </tr>
            <tr><td colspan="4" style="padding-top: 10px;">   &copy; {{ date('Y') }} Guess Posting Portal. All rights reserved.</td></tr>
        </table>

    @endcomponent
@endslot

@endcomponent


<style>
    .social-links {
        text-align: center;
    }
    
    .social-links td img{
        width: 30px;
        }
    td.panel-content {
    padding: 0 !important;
    
    background-color: transparent !important;
    
    }
    .header a div{
    width:100%;
    padding: 15px !important;
    color: #3c6daf;
    }
    .header a{
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    width:100%
    }
    table.footer ,
    td.header {
    width: 100% !important;
    background: #ffffff;
    }
    td.header {
    padding: 0px !important;
    padding-top: 15px !important;
    
    border-bottom: 2px solid #3c6daf;
    
    }
    
    table.footer .content-cell {
        padding: 20px !important;
        background-color: #4f93ef;
        color: #fff;
    }
    table.footer,
    table.content {
        width: 100%;
        max-width: 500px;
    }
    
    tr td table.footer {
        max-width: 570px !important;
    }
    
    
    
    table.footer p,
    td.header a {
    color: #3c6daf !important;
    text-shadow: unset !important;
    }
    table.subcopy th,
    table.subcopy td {
        padding: 5px;
        text-align: left;
    }
    
    td.header, td.header a {
        background: #3c8efe;
        padding-top: 20px;
    }
    td.header{
        padding-top: 0px !Important
    }
    .content-cell h1 {
        font-size: 13px !Important;
        font-weight: 600 !Important;
        margin-top: 0 !Important;
        text-align: left !Important;
    }
    .content-cell h2 {
        color: #ffffff !Important;
        background: #FF9800 !Important;
        padding: 8px !Important;
        text-align: center !Important;
    }
    </style>
