@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
                <small>Add User</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('users.index')}}">Users</a></li>
                <li class="active">Add Users</li>
            </ol>

        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">

                        <div class="box-header with-border">
                            {{-- <h3 class="box-title"><i class='fa fa-user-plus'></i> Add User</h3>--}}
                            <div class="col-md-6">
                                {{ Form::open(array('url' => 'users')) }}

                                <div class="form-group">
{{--                                    <label class="col-form-label">Name</label>--}}
{{--                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="">--}}
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', '', array('class' => 'form-control @error("name") is-invalid @enderror')) }}

                                    @error('name')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
{{--                                    <label class="col-form-label">Email</label>--}}
{{--                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="">--}}
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::email('email', '', array('class' => 'form-control @error("email") is-invalid @enderror')) }}

                                    @error('email')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror


                                </div>

                                <div class='form-group'>
                                    @foreach ($roles as $role)
                                        {{ Form::checkbox('roles[]',  $role->id ) }}
                                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                                    @endforeach
                                </div>

                                <div class="form-group">
{{--                                    <label class="col-form-label">Password</label>--}}
{{--                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="">--}}
                                    {{ Form::label('password', 'Password') }}<br>
                                    {{ Form::password('password', array('class' => 'form-control @error("password") is-invalid @enderror')) }}

                                    @error('password')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>

                                <div class="form-group">
{{--                                    <label class="col-form-label">Confirm Password</label>--}}
{{--                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="">--}}
                                    {{ Form::label('password', 'Confirm Password') }}<br>
                                    {{ Form::password('password_confirmation', array('class' => 'form-control @error("password_confirmation") is-invalid @enderror')) }}

                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                {{ Form::submit('Add User', array('class' => 'btn bg-navy')) }}

                                {{ Form::close() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
@endsection
