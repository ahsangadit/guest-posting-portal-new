-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2019 at 01:58 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guest-posting-portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_meta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_amount` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_details`, `user_meta`, `created_at`, `updated_at`, `total_amount`, `status`) VALUES
(4, 1, 'a:2:{i:0;a:9:{s:5:\"title\";s:6:\"Medium\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:14:\"<p>Testing</p>\";s:5:\"price\";s:3:\"200\";s:2:\"da\";s:2:\"20\";s:2:\"fb\";s:3:\"200\";s:8:\"follower\";s:4:\"1998\";s:7:\"blog_id\";s:1:\"5\";s:7:\"user_id\";s:1:\"1\";}i:1;a:9:{s:5:\"title\";s:6:\"title1\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:26:\"<p>this is description</p>\";s:5:\"price\";s:2:\"46\";s:2:\"da\";s:3:\"102\";s:2:\"fb\";s:2:\"10\";s:8:\"follower\";s:2:\"10\";s:7:\"blog_id\";s:1:\"4\";s:7:\"user_id\";s:1:\"1\";}}', 'a:9:{s:6:\"_token\";s:40:\"c1AVnf0tHeDvJjgO7Bk7UExtwikASLPFb1GlO1TJ\";s:5:\"fname\";s:5:\"ahsan\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:7:\"testing\";s:7:\"country\";s:7:\"testing\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:11:\"43534543534\";s:5:\"notes\";s:4:\"test\";}', '2019-12-23 06:29:05', '2019-12-23 06:29:05', 246, 'pending'),
(5, 3, 'a:2:{i:0;a:9:{s:5:\"title\";s:6:\"Medium\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:14:\"<p>Testing</p>\";s:5:\"price\";s:3:\"200\";s:2:\"da\";s:2:\"20\";s:2:\"fb\";s:3:\"200\";s:8:\"follower\";s:4:\"1998\";s:7:\"blog_id\";s:1:\"5\";s:7:\"user_id\";s:1:\"3\";}i:1;a:9:{s:5:\"title\";s:6:\"title1\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:26:\"<p>this is description</p>\";s:5:\"price\";s:2:\"46\";s:2:\"da\";s:3:\"102\";s:2:\"fb\";s:2:\"10\";s:8:\"follower\";s:2:\"10\";s:7:\"blog_id\";s:1:\"4\";s:7:\"user_id\";s:1:\"3\";}}', 'a:9:{s:6:\"_token\";s:40:\"Doeh3SqRBX0eu4COE2EvwNq6XpH8krkAYtZcFXZ0\";s:5:\"fname\";s:3:\"abc\";s:5:\"lname\";s:3:\"abc\";s:7:\"address\";s:3:\"abc\";s:4:\"city\";s:3:\"abc\";s:7:\"country\";s:3:\"abc\";s:9:\"post_code\";s:3:\"abc\";s:12:\"phone_number\";s:3:\"abc\";s:5:\"notes\";s:3:\"abc\";}', '2019-12-23 06:56:04', '2019-12-23 06:56:04', 246, 'pending');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
