<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class keyword extends Model
{   
    protected $table = 'keywords';
    protected $fillable = ['id', 'keyword'];


    public function blogs(){
        return $this->belongsToMany(blog::class,'blog_keywords');
    }

}
