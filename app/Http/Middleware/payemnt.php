<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use auth;

class payemnt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if (Auth::check() && !Auth::user()->hasRole('admin') && ($user->credit == "no") ) {
            return redirect()->route('addmoney.paywithpaypal');
        }
        else{
            return $next($request);
           
        
        }
    }
}
