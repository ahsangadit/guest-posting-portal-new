<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\blog;
use App\blog_meta;
use App\keyword;
use App\blog_keyword;
use App\Industry;
use App\blog_industry;
use App\orders;
use App\order_blog;
use App\order_log;
use Response;
use Auth;
use Mail;
use DB;
use App\Mail\CheckoutDetails;
use Illuminate\Support\Facades\Input;

class BlogController extends Controller
{
    public function __construct()
    {
        //  $this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }


    public function index()
    {
        $blogs = blog::with('blog_meta')->get();
        $blog_meta_details = [];

        foreach ($blogs as $kk => $blog) {
            foreach ($blog->blog_meta as $key => $blog_meta) {
                $blog_meta_details[$blog_meta->meta_key] = $blog_meta->meta_value;
            }
        }

        return view('backend.blogs.all', ['blog' => $blogs, 'blog_meta_details' => $blog_meta_details , 'fields_value' => null]);
    }


    public function create()
    {

        $keywords = keyword::select('id', 'keyword')->get();
        $industries = Industry::select('id', 'industry')->get();
        $all_industries = [];
        $all_keywords = [];
        foreach ($keywords as $keyword) {
            $all_keywords[$keyword->id] = $keyword->keyword;
        }
        foreach ($industries as $industry) {
            $all_industries[$industry->id] = $industry->industry;
        }
        return view('backend.blogs.add', ['keywords' => $all_keywords, 'industries' => $all_industries]);
    }

    public function store(Request $request)
    {
        $blog = new blog();


        $custom_message = [
            'da' => 'The DA is required',
            'fb_likes' => 'The PA is required',
            'follower' => 'The Organic Traffic is required',
            'dropped' => 'The Dropped is required',
            'image' => 'The Image is required'
        ];

        $this->validate($request, [

            'title' => 'required',
            'da' => 'required',
            'fb_likes' => 'required',
            'follower' => 'required',
            'dropped' => 'required',
            'link' => 'required',
            'industry' => 'required',
            'keyword' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'required|image'
        ], $custom_message);

        $file = $request->file('image');
        $extension = $request->image->extension();

        $blog->link = $request['link'];
        $blog->title = $request['title'];
        $blog->description = $request['description'];
        $blog->price = $request['price'];
        $blog->blog_image = $request->image->store('upload', 'public');

        //$blog->save();
        if ($blog->save()) {
            $blog_metas = blog_meta::insert(array(
                array('blog_id' => $blog->id, 'meta_key' => 'da', 'meta_value' => $request['da']),
                array('blog_id' => $blog->id, 'meta_key' => 'fb_likes', 'meta_value' => $request['fb_likes']),
                array('blog_id' => $blog->id, 'meta_key' => 'follower', 'meta_value' => $request['follower']),
                array('blog_id' => $blog->id, 'meta_key' => 'dropped', 'meta_value' => $request['dropped']),
            ));
            foreach ($request['keyword'] as $keyword) {
                $blog_keyword = new blog_keyword();
                $blog_keyword->blog_id = $blog->id;
                $blog_keyword->keyword_id = $keyword;
                $blog_keyword->save();
            }
            foreach ($request['industry'] as $industry) {
                $blog_industry = new blog_industry();
                $blog_industry->blog_id = $blog->id;
                $blog_industry->industry_id = $industry;
                $blog_industry->save();
            }
        }

        return redirect()->route('blogs.bloglist')
            ->with(
                'flash_message',
                'Blog ' . $blog->title . ' added!'
            );
    }


    public function show()
    {
        $blogs = blog::with('blog_meta')->get();
        $blog_meta_details = [];

        foreach ($blogs as $blog) {
            foreach ($blog->blog_meta as $key => $blog_meta) {
                $blog_meta_details[$blog_meta->meta_key] = $blog_meta->meta_value;
            }
        }

        return view('backend.blogs.blogs', ['blogs' => $blogs, 'blog_meta_details' => $blog_meta_details]);
    }

    public function viewDetails($id)
    {

        $blog = blog::find($id);
        $metas = blog_meta::where('blog_id', $blog->id)->get();
        $blog_keywords = blog_keyword::where('blog_id', $blog->id)->get();
        $blog_industries = blog_industry::where('blog_id', $blog->id)->get();

        $blog_metas = [];

        foreach ($metas as $meta) {
            $blog_metas[$meta->meta_key] = $meta->meta_value;
        }

        return view('backend.blogs.show', ['blog' => $blog, 'blog_keyword' => $blog_keywords, 'blog_industries' => $blog_industries, 'blog_metas' => $blog_metas]);
    }


    public function edit($id)
    {
        //
        $blog = blog::find($id);
        $keywords = keyword::all();
        $industries = Industry::all();
        $all_industries = [];
        $all_keywords = [];

        foreach ($keywords as $keyword) {
            $all_keywords[$keyword->id] = $keyword->keyword;
        }

        foreach ($industries as $industry) {
            $all_industries[$industry->id] = $industry->industry;
        }

        $blog_metas = blog_meta::where('blog_id', $blog->id)->get();
        $blog_keywords = blog_keyword::where('blog_id', $blog->id)->get();
        $blog_industries = blog_industry::where('blog_id', $blog->id)->get();

        foreach ($blog_metas as $blog_meta) {
            $blog[$blog_meta->meta_key] = $blog_meta->meta_value;
        }

        $keyword = [];
        foreach ($blog_keywords as $blog_keyword) {
            $keyword[] = $blog_keyword->keyword_id;
        }

        $blog['keyword'] = $keyword;

        $industry = [];

        foreach ($blog_industries as $blog_industry) {
            $industry[] = $blog_industry->industry_id;
        }

        $blog['industry'] = $industry;
        //        dd($blog);

        return view('backend.blogs.edit', ['blog' => $blog, 'keywords' => $all_keywords, 'industries' => $all_industries]);
    }


    public function update(Request $request, $id)
    {

        $blogs = blog::findOrFail($id);

        $this->validate($request, [
            // 'title' => 'required',
            // 'description' => 'required',
            // 'da' => 'required',
            // 'fb_likes' => 'required',
            // 'follower' => 'required',
            // 'dropped' => 'required',
        ]);

        $file = $request->file('image');
        $extension = $request->image->extension();


        $blogs->link = $request['link'];
        $blogs->title = $request['title'];
        $blogs->description = $request['description'];
        $blogs->price = $request['price'];

        $blogs->blog_image = $request->image->store('upload', 'public');


        if ($blogs->save()) {
            blog_meta::where('blog_id', $blogs->id)->where('meta_key', 'da')
                ->update([
                    'meta_key' => 'da',
                    'meta_value' => $request['da'],
                ]);
            blog_meta::where('blog_id', $blogs->id)->where('meta_key', 'fb_likes')
                ->update([
                    'meta_key' => 'fb_likes',
                    'meta_value' => $request['fb_likes'],
                ]);
            blog_meta::where('blog_id', $blogs->id)->where('meta_key', 'follower')
                ->update([
                    'meta_key' => 'follower',
                    'meta_value' => $request['follower'],
                ]);
            blog_meta::where('blog_id', $blogs->id)->where('meta_key', 'dropped')
                ->update([
                    'meta_key' => 'dropped',
                    'meta_value' => $request['dropped'],
                ]);
            blog_keyword::where('blog_id', $blogs->id)->delete();
            foreach ($request['keyword'] as $keyword) {
                $blog_keyword = new blog_keyword();
                $blog_keyword->blog_id = $blogs->id;
                $blog_keyword->keyword_id = $keyword;
                $blog_keyword->save();
            }
            blog_industry::where('blog_id', $blogs->id)->delete();
            foreach ($request['industry'] as $industry) {
                $blog_industry = new blog_industry();
                $blog_industry->blog_id = $blogs->id;
                $blog_industry->industry_id = $industry;
                $blog_industry->save();
            }
        }

        return redirect()->route('blogs.bloglist')
            ->with(
                'flash_message',
                'Blog ' . $blogs->title . ' updated!'
            );
    }


    public function destroy($id)
    {
        //
        $blogs = blog::findOrFail($id);

        //Make it impossible to delete this specific permission

        $blogs->delete();

        return redirect()->route('blogs.bloglist')
            ->with(
                'flash_message',
                'blog deleted!'
            );
    }


    public function blog_search()
{

    $keyword = Input::get('keyword');

    //dd($keyword);
    //$get_keyword = keyword::where('keyword', 'LIKE', '%' . $keyword . '%')->first();

    $da = Input::get('da');
    $fb_likes = Input::get('fb_likes');
    $follower = Input::get('follower');
    $price = Input::get('price');

    $field_values = array('keyword'=>$keyword , 'da'=> $da , 'fb_likes' => $fb_likes , 'follower' => $follower , 'price' => $price);

    $da         = array_map('intval', array_map('trim', explode('-', $da)));
    $fb_likes   = array_map('intval', array_map('trim', explode('-', $fb_likes)));
    $follower   = array_map('intval', array_map('trim', explode('-', $follower)));
    $price      = array_map('intval', array_map('trim', explode('-', $price)));

    $mainResult = [];

    $da_result         = blog_meta::where('meta_key', 'da')->whereBetween('meta_value', $da)->pluck('blog_id')->toArray();
    $fb_likes_result   = blog_meta::where('meta_key', 'fb_likes')->whereBetween('meta_value', $fb_likes)->pluck('blog_id')->toArray();
    $follower_result   = blog_meta::where('meta_key', 'follower')->whereBetween('meta_value', $follower)->pluck('blog_id')->toArray();
    $price_result      = blog::whereBetween('price', $price)->pluck('id')->toArray();

    $da_result          = (!empty($da_result) ? $da_result : array() );
    $fb_likes_result    = (!empty($fb_likes_result) ? $fb_likes_result : array() );
    $follower_result    = (!empty($follower_result) ? $follower_result : array() );
    $price_result       = (!empty($price_result) ? $price_result : array() );

    $keyword_result = [];
    if(!empty($keyword)){
        $keyword   = keyword::where('keyword', 'LIKE', '%' . $keyword . '%')->first();
        //customVarDump($keyword);

        if(!empty($keyword)){
            $keywords  = blog_keyword::select('blog_id')->where('keyword_id', $keyword->id)->with('blog')->get();
            foreach ($keywords as $key => $val ){
                $keyword_result[] = $val->blog_id;
            }
        }
    }

//        echo"da";
//        customVarDump($da_result);
//
//        echo"FB likes";
//        customVarDump($fb_likes_result);
//
//        echo"Follower";
//        customVarDump($follower_result);
//
//        echo"Price";
//        customVarDump($price_result);
//
//        echo"Keywords";
//        customVarDump($keyword_result);


//        $ids = array_intersect($da_result , $fb_likes_result, $follower_result , $price_result);
    $mergeIDs = array_merge($da_result , $fb_likes_result, $follower_result , $price_result , $keyword_result);
    $ids = array_unique($mergeIDs);

//        echo "<br>"."==RESULT";
//        customVarDump_die($ids);

    return view('backend.blogs.all', ['result' => $ids , 'fields_value' => $field_values]);

}



    public function add_to_cart(Request $request)
    {
        $log_details = order_log::all();
        $new_data = $request->all();
        $current_user_id = $new_data['user_id'];

        if (count($log_details) == 0) {
            //echo "no record found";
            $log = new order_log();
            $arr_ = array($new_data);
            $log->user_id = $current_user_id;
            $log->meta_key = 'add-to-cart-details';
            $log->meta_value = serialize($arr_);
            $log->save();
        } else {

            $userIDs = [];
            $main_arr = [];
            $ids = DB::table('order_log')->pluck('user_id');

            foreach ($ids as $user_id) {
                $userIDs[] = $user_id;
            }


            if (in_array(Auth::user()->id, $userIDs)) {

                foreach ($log_details as $log_detail) {

                    $user_id_ = (!empty($log_detail->user_id) ? $log_detail->user_id : "0");

                    if ($user_id_ == $current_user_id) {

                        $previous_logs = unserialize($log_detail->meta_value);
                        $prev_arrays = count($previous_logs);


                        $new_logs = $new_data;
                        $num = 1;
                        $main_arr[0] = $new_logs;
                        // dd($previous_logs);
                        for ($i = 1; $i <= $prev_arrays; $i++) {
                            foreach ($previous_logs as $key => $arr) {
                                $main_arr[$i] = $arr;
                                $i++;
                            }
                        }


                    }
                }

                order_log::where(['user_id' => Auth::user()->id])->update([
                    "meta_key" => 'add-to-cart-details',
                    "meta_value" => serialize($main_arr)
                ]);
            } else {
                $log = new order_log();
                $array_ = array($new_data);
                $log->user_id = $current_user_id;
                $log->meta_key = 'add-to-cart-details';
                $log->meta_value = serialize($array_);
                $log->save();
            }
        }
    }




    public function add_to_cart_update()
    {
        $current_user_id = Auth::user()->id;
        $log_details = order_log::where('user_id', $current_user_id)->get();
        // dd($log_details);

        $cart_no = 0;
        foreach ($log_details as $log) {
            //    $user_id = $log->user_id;
            $cart_no = count(unserialize($log->meta_value));
        }
        //echo $cart_no;
        // $cart_no = !($cart_no) ? $cart_no : "0";

        return Response::json(array('success' => true, 'result' => $cart_no));
    }

    public function cart(Request $request)
    {
        $current_user_id = Auth::user()->id;
        

        $log_details = order_log::where('user_id', $current_user_id)->get();
        $log_details = unserialize($log_details[0]->meta_value);
        $arr = [];
        $total_amount = 0;
        $total_blogs = count($log_details);

    

        foreach ($log_details as $key => $data) {
            // $arr[] = sort($data);
            // $total_amount += $data['price'];
            $arr[] = $data['price'];

        }

      
        return view('backend.cart', ['cart_data' => $log_details, 'total_amount' => $arr, 'total_blogs' => $total_blogs]);
    }


    public function checkout(Request $request)
    {

        $current_user_id = Auth::user()->id;
        $user_detail = Auth::user();
        $log_details = order_log::where('user_id', $current_user_id)->get();
        $log_details = unserialize($log_details[0]->meta_value);
        $arr = [];
        $arr1 = [];
        foreach ($log_details as $data) {
            //  $arr[] = sort($data);
            $arr1[] = $data['price'];
        }
        $total_price = array_sum($arr1);
        Mail::to('guest-posting-portal@hztech.biz')->send(new CheckoutDetails($log_details,$user_detail));
        return view('backend.checkout', ['cart_data' => $log_details, 'total_price' => $total_price]);
       
    }


    public function cart_update(Request $request)
    {
        $current_user_id = Auth::user()->id;
        $log_details = order_log::where('user_id', $current_user_id)->get();
        $log_details = unserialize($log_details[0]->meta_value);
        //dd($log_details[$request->array_index]);
        // dd('update');
        //dd($request->all());

        return redirect()->route('blog.cart');
    }


    public function cart_delete(Request $request)
    {
        $current_user_id = Auth::user()->id;
        $log_details = order_log::where('user_id', $current_user_id)->get();
        $log_details = unserialize($log_details[0]->meta_value);
        unset($log_details[$request->array_index]);
        $log_details = serialize($log_details);
        order_log::where(['user_id' => Auth::user()->id])->update([
            "meta_key" => 'add-to-cart-details',
            "meta_value" => $log_details
        ]);
        return redirect()->route('blog.cart');
    }

    // public function blog_list(){
    //     dd('ahsa');
    //     $blogs = blog::with('blog_meta')->get();
    //     $blog_meta_details = [];
    //             dd($blogs);

    //     foreach ($blogs as $blog) {
    //         foreach ($blog->blog_meta as $key => $blog_meta) {
    //             $blog_meta_details[$blog_meta->meta_key] = $blog_meta->meta_value;
    //         }
    //     }

    //     return view('backend.blogs.blogs', ['blogs' => $blogs , 'blog_meta_details' => $blog_meta_details]);
    //    }


}
