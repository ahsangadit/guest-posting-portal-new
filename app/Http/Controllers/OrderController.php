<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\orders;
use App\User;
use App\order_log;
use Auth;
use App\Mail\CreateOrder;
use App\Mail\UpdateOrderStatus;
use Mail;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\URL;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class OrderController extends Controller
{
    private $_api_context;



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(orders $orders)
    {
         $this->orders            =   $orders;
         $paypal_conf = \Config::get('paypal');
         $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
         $this->_api_context->setConfig($paypal_conf['settings']);

     }

    public function index()
    {
        $user = Auth::user();
        if($user->hasRole(1)){
            $orders = orders::latest()->get();
        }
        else{
            $current_user_id = Auth::user()->id;
            $orders = orders::where('user_id',$current_user_id)->latest()->get();
        }


        // $orders = orders::all();
        // foreach ($orders as $order)
        // {
        //     $user = User::find($order->user_id);
        //     $order['username'] = $user->name;
        // }

        return view('backend.orders.all',['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $custom_message = [
            'fname.required'         => 'The first name is required',
            'lname.required'         => 'The last name is required',
            'address.required'       => 'The address is required',
            'city.required'          => 'The city is required',
            'country.required'       => 'The country is required',
            'post_code.required'     => 'The post code is required',
            'phone_number.required'  => 'The phone number is required',
            'website_url.required'   => 'The URL is required',
            'anchor.required'        => 'The Anchor is required',
            'wordcount.required'     => 'The Word Count is required',
            'image_count.required'   => 'The Image Count is required',

        ];

        $this->validate($request, [

             'fname'        => 'required',
             'lname'        => 'required',
             'address'      => 'required',
             'city'         => 'required',
             'country'      => 'required',
             'post_code'    => 'required',
             'phone_number' => 'required',
             'email'        => 'required',
             'website_url'  => 'required',
             'anchor'       => 'required',
             'wordcount'    => 'required',
             'image_count'  => 'required'

        ],$custom_message);

         $log_details        = order_log::where('user_id',  $user->id)->get();
        // $log_details1       = unserialize( $log_details[0]->meta_value);
         $log_details_paypal = unserialize( $log_details[0]->meta_value);

        // $email             =   $request->email;
        // $user_detail       =   $request->all();
         

        //  $amount=[];
 
        //  foreach($log_details1 as $data){
        //      $amount[]=$data['price']; 
  
        //  }

        //  $total_amount  =   array_sum($amount);
        //  $email         =   $email;

        //  $user_detail   = serialize($user_detail);
        //  $log_details2  = serialize($log_details1);

        //  $order                 = new orders();
        //  $order->user_id        = $user->id;
        //  $order->user_meta      = $user_detail;
        //  $order->status         = "pending";
        //  $order->total_amount   = $total_amount;
        //  $order->order_details  = $log_details2;
        //  $order->user_email     = $email;

        //  $order->save();

        //  Mail::to($email)->send(new CreateOrder($log_details_paypal));
        //  order_log::where('user_id',  $user->id)->delete();

         $payer                 = new Payer();
         $payer->setPaymentMethod('paypal');

         $item_1                = new Item();
         $arr=[];

         // $custom_amount = $request->get('amount');
         if($request->get('amount')) {
         $custom_amount = $request->get('amount');

         foreach($log_details_paypal as $data){

            $item_1     = new Item();
            $arr[]      = $item_1->setName($data['title'])/** item name **/
            ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($data['price']);
            /** unit price **/

         }

         $item_list     = new ItemList();
         $item_list->setItems($arr);
 

         $amount        = new Amount();
         $amount->setCurrency('USD')
                ->setTotal($custom_amount);
         
         $transaction   = new Transaction();
         $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
            // dd($transaction);
        $redirect_urls  = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('orders.status',$request->all() ))/** Specify return URL **/
            ->setCancelUrl(route('orders.status',$request->all()));

        $payment        = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/

        try {

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('addmoney.paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('addmoney.paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {
            /** redirect to paypal **/

            return Redirect::away($redirect_url);
        }

            \Session::put('error', 'Unknown error occurred');
            return Redirect::route('addmoney.paywithpaypal');

        }

        //return redirect()->route('orders.index');

    }

    public function getOrderStatus(Request $request)
    {
        $check_out[] = $request->all();
        $user        = Auth::user();

 

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error','Payment failed');
            return Redirect::route('addmoney.paywithpaypal');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/

       
        if ($result->getState() == 'approved') {

            $log_details        = order_log::where('user_id',  $user->id)->get();
            
            $log_details1       = unserialize($log_details[0]->meta_value);
            $log_details_paypal = unserialize( $log_details[0]->meta_value);
            
   
           
             $amount=[];
     
             foreach($log_details1 as $data){
                 $amount[]=$data['price'];
             }
    
             $total_amount  =   array_sum($amount);
             $email= $check_out[0]['email'];
             $user_detail   = serialize($check_out);
             $log_details2  = serialize($log_details1);

                $order                 = new orders();
                $order->user_id        = $user->id;
                $order->user_meta      = $user_detail;
                $order->status         = "pending";
                $order->total_amount   = $total_amount;
                $order->order_details  = $log_details2;
                $order->user_email     = $email;
                $order->paypal_all_response     = $result;

                $order->save();

         Mail::to($email)->send(new CreateOrder($log_details_paypal));
         order_log::where('user_id',  $user->id)->delete();
    
            // dd($result);
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            // User::where(['id' => $user->id ])->update([
            //     "credit" => "yes"
            // ]);

            \Session::put('success','Payment success');

            return Redirect::route('blogs.index');
            //return Redirect::route('addmoney.paywithpaypal');
        }
        \Session::put('error','Payment failed');
        return Redirect::route('addmoney.paywithpaypal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = orders::where('id',$id)->get();
    
        $order_status = $order[0]['status'];
        $total = $order[0]['total_amount'];
    
        $user_detail = unserialize($order[0]['user_meta']);
        $order = unserialize($order[0]['order_details']);
        $total_blogs = count($order);
        return view('backend.orders.show',['order'=>$order,'user_detail' => $user_detail , 'order_status' => $order_status , 'total' => $total ,'total_blogs' => $total_blogs ]);
    }   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $order = orders::where('id',$id)->get();


        $order_full_detail = orders::where('id',$id)->get();

        $order_status = $order[0]['status'];
        $total = $order[0]['total_amount'];

        $user_detail = unserialize($order[0]['user_meta']);
        $order = unserialize($order[0]['order_details']);
        $total_blogs = count($order);

        return view('backend.orders.edit',['order_full_detail'=>$order_full_detail,'order'=>$order,'user_detail' => $user_detail , 'order_status' => $order_status , 'total' => $total ,'total_blogs' => $total_blogs ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newstatus=$request->status;
        $order_new_status  =   $this->orders->find($id);
        $order_detail   = unserialize($order_new_status->order_details);
  
        $email = $order_new_status->user_email;
        $order_new_status->status = $newstatus;
        if($order_new_status->save()){
            Mail::to($email)->send(new UpdateOrderStatus($order_detail,$order_new_status));
        }
        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = orders::where('id',$id)->delete();
        return redirect()->route('orders.index')->with(
            'flash_message',
            'Order Deleted Successfully '
        );

    }
}
