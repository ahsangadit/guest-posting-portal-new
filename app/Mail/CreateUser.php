<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data1,$data2,$data3)
    {
        $this->data1=$data1;
        $this->data2=$data2;
        $this->data3=$data3;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('guest-posting-portal@hztech.biz')->subject('User Created Successfully')->markdown('backend.email.create_user')->with(['data1'=>$this->data1,'data2'=>$this->data2,'data3'=>$this->data3]);
    }
}
