<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    //
    protected $table = 'industries';
    protected $fillable = ['id', 'industry'];

    public function blogs(){
        return $this->belongsToMany(blog::class,'blog_industries');
    }
}
