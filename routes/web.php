<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

    Route::get('/home', 'HomeController@index')->name('Home');

    Route::group(['middleware' => ['isAdmin', 'auth']], function() {

        Route::get('users/allUsers', 'UserController@index')->name('users.allusers');
        Route::resource('users', 'UserController');


        Route::get('roles/allRoles', 'RoleController@index')->name('roles.allroles');
        Route::resource('roles', 'RoleController');


        Route::get('permissions/allPermissions', 'PermissionController@index')->name('permissions.allpermissions');
        Route::resource('permissions', 'PermissionController');


        Route::get('keywords/allKeywords', 'KeywordController@index')->name('keywords.allkeywords');
        Route::resource('keywords', 'KeywordController');


        Route::get('industry/allIndustries', 'IndustryController@index')->name('industry.allindustries');
        Route::resource('industry', 'IndustryController');

    });
   
    Route::group(['prefix' => 'blogs', 'as' => 'blogs','middleware'=>'auth'],function (){

        Route::get('/','BlogController@index')->name('.index')->middleware('payment');

        Route::get('/allBlogs','BlogController@index')->name('.allblogs')->middleware('payment');
        Route::post('/searchresult', 'BlogController@blog_search')->name('.searchresult');

        Route::get('/list','BlogController@show')->name('.bloglist');
        Route::post('/store', 'BlogController@store')->name('.store');
        Route::get('/{id}/edit', 'BlogController@edit')->name('.edit');
        Route::get('/create','BlogController@create')->name('.create');
        Route::get('/{id}/delete', 'BlogController@destroy')->name('.destroy');
        Route::post('/{id}/update', 'BlogController@update')->name('.update');
        Route::post('/add_to_cart','BlogController@add_to_cart')->name('.add_to_cart');
        Route::post('/add_to_cart_update','BlogController@add_to_cart_update')->name('.add_to_cart_update');
        Route::get('/viewDetails/{id}', 'BlogController@viewDetails')->name('.viewDetails');
     });
        
   // Route::resource('blogs', 'BlogController')->middleware('isAdmin');
    // Route::get('/blogs', 'BlogController@index')->name('blogs.index')->middleware('auth','payment');
    // Route::get('/blogs/list', 'BlogController@show')->name('blogs.bloglist')->middleware('isAdmin');
    // Route::post('/blogs/searchresult','BlogController@blog_search')->name('blogs.searchresult')->middleware('auth');;
    // Route::post('/blogs/add_to_cart','BlogController@add_to_cart')->name('blogs.add_to_cart');
    // Route::post('/blogs/add_to_cart_update','BlogController@add_to_cart_update')->name('blogs.add_to_cart_update');


    Route::get('/cart','BlogController@cart')->name('blog.cart')->middleware('auth');
    Route::post('/cart_update','BlogController@cart_update')->name('blog.cart_update');
    Route::post('/cart_delete','BlogController@cart_delete')->name('blog.cart_delete');
    Route::get('/checkout','BlogController@checkout')->name('blog.checkout')->middleware('auth');
      
    Route::resource('orders', 'OrderController')->middleware('auth');
    //Route::post('/orders/update/{id}', 'OrderController@update')->name('orders.update');
    Route::get('paywithpaypal', array('as' => 'addmoney.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
    Route::post('paypal', array('as' => 'addmoney.paypal','uses' => 'AddMoneyController@postPaymentWithpaypal',));
    Route::get('paypal', array('as' => 'payment.status','uses' => 'AddMoneyController@getPaymentStatus',));
    Route::get('/orders/create/{array}', array('as' => 'orders.status','uses' => 'OrderController@getOrderStatus',));


    Route::get('/setting', array('as' => 'user.setting' , 'uses' => 'UserController@settingView'))->middleware('auth');
    Route::get('/changePassword' , array('as' => 'user.changePassword' , 'uses' => 'UserController@ChangePassView'))->middleware('auth');
    Route::post('/users/{user}' , array('as' => 'users.updatePassword' , 'uses' => 'UserController@updatePassword'))->middleware('auth');
    Route::GET('/testing' , array('as' => 'users.testing' , 'uses' => 'UserController@testing'))->middleware('auth');
