-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2019 at 02:14 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guest-posting-portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `link`, `title`, `description`, `created_at`, `updated_at`, `price`) VALUES
(4, 'https://www.google.com/', 'title1', '<p>this is description</p>', '2019-07-29 11:05:41', '2019-07-31 06:54:51', 46),
(5, 'https://medium.com/', 'Medium', '<p>Testing</p>', '2019-12-18 04:22:36', '2019-12-18 04:22:36', 200),
(6, 'facebook.com', 'ABC', '<p>ABC<br></p>', '2019-12-18 06:16:07', '2019-12-18 06:16:07', 32);

-- --------------------------------------------------------

--
-- Table structure for table `blog_industries`
--

CREATE TABLE `blog_industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_industries`
--

INSERT INTO `blog_industries` (`id`, `blog_id`, `industry_id`, `created_at`, `updated_at`) VALUES
(2, 4, 1, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(3, 5, 1, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(4, 6, 1, '2019-12-18 06:16:07', '2019-12-18 06:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `blog_keywords`
--

CREATE TABLE `blog_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `keyword_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_keywords`
--

INSERT INTO `blog_keywords` (`id`, `blog_id`, `keyword_id`, `created_at`, `updated_at`) VALUES
(8, 4, 1, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(9, 4, 2, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(10, 5, 1, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(11, 5, 2, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(12, 6, 1, '2019-12-18 06:16:07', '2019-12-18 06:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `blog_metas`
--

CREATE TABLE `blog_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_metas`
--

INSERT INTO `blog_metas` (`id`, `blog_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(6, 4, 'da', '102', NULL, '2019-12-18 02:46:36'),
(7, 4, 'fb_likes', '10', NULL, '2019-12-18 02:46:36'),
(8, 4, 'follower', '10', NULL, '2019-12-18 02:46:36'),
(9, 4, 'dropped', 'true', NULL, '2019-12-18 02:46:36'),
(10, 4, 'flag', 'true', NULL, '2019-07-29 11:05:51'),
(11, 5, 'da', '20', NULL, NULL),
(12, 5, 'fb_likes', '200', NULL, NULL),
(13, 5, 'follower', '1998', NULL, NULL),
(14, 5, 'dropped', 'false', NULL, NULL),
(15, 6, 'da', '21', NULL, NULL),
(16, 6, 'fb_likes', '323', NULL, NULL),
(17, 6, 'follower', '33', NULL, NULL),
(18, 6, 'dropped', 'false', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit_histories`
--

CREATE TABLE `credit_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `industry`, `created_at`, `updated_at`) VALUES
(1, 'Information Technology', '2019-07-31 09:50:19', '2019-07-31 09:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `keyword`, `created_at`, `updated_at`) VALUES
(1, 'Cars', '2019-07-30 10:47:07', '2019-07-30 10:47:07'),
(2, 'Shopify', '2019-12-18 02:45:38', '2019-12-18 02:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_19_105232_create_blogs_table', 1),
(4, '2019_07_19_105519_create_blog_metas_table', 1),
(5, '2019_07_19_105618_create_keywords_table', 1),
(6, '2019_07_19_113108_create_user_metas_table', 1),
(7, '2019_07_19_113601_create_blog_keywords_table', 1),
(8, '2019_07_19_115159_create_user_blogs_table', 1),
(9, '2019_07_19_115454_create_orders_table', 1),
(10, '2019_07_19_120831_create_order_blogs_table', 1),
(11, '2019_07_19_120933_create_credit_histories_table', 1),
(12, '2019_07_22_085655_create_permission_tables', 1),
(13, '2019_07_31_114717_add_price_to_blog_table', 2),
(14, '2019_07_31_114907_add_total_amount_to_orders_table', 2),
(15, '2019_07_31_132235_add_status_to_orders_table', 3),
(16, '2019_07_31_140818_create_industry_table', 4),
(19, '2019_07_31_140901_create_blog_industry_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_meta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_amount` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_details`, `user_meta`, `created_at`, `updated_at`, `total_amount`, `status`) VALUES
(4, 1, 'a:2:{i:0;a:9:{s:5:\"title\";s:6:\"Medium\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:14:\"<p>Testing</p>\";s:5:\"price\";s:3:\"200\";s:2:\"da\";s:2:\"20\";s:2:\"fb\";s:3:\"200\";s:8:\"follower\";s:4:\"1998\";s:7:\"blog_id\";s:1:\"5\";s:7:\"user_id\";s:1:\"1\";}i:1;a:9:{s:5:\"title\";s:6:\"title1\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:26:\"<p>this is description</p>\";s:5:\"price\";s:2:\"46\";s:2:\"da\";s:3:\"102\";s:2:\"fb\";s:2:\"10\";s:8:\"follower\";s:2:\"10\";s:7:\"blog_id\";s:1:\"4\";s:7:\"user_id\";s:1:\"1\";}}', 'a:9:{s:6:\"_token\";s:40:\"c1AVnf0tHeDvJjgO7Bk7UExtwikASLPFb1GlO1TJ\";s:5:\"fname\";s:5:\"ahsan\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:7:\"testing\";s:7:\"country\";s:7:\"testing\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:11:\"43534543534\";s:5:\"notes\";s:4:\"test\";}', '2019-12-23 06:29:05', '2019-12-23 06:29:05', 246, 'pending'),
(5, 3, 'a:2:{i:0;a:9:{s:5:\"title\";s:6:\"Medium\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:14:\"<p>Testing</p>\";s:5:\"price\";s:3:\"200\";s:2:\"da\";s:2:\"20\";s:2:\"fb\";s:3:\"200\";s:8:\"follower\";s:4:\"1998\";s:7:\"blog_id\";s:1:\"5\";s:7:\"user_id\";s:1:\"3\";}i:1;a:9:{s:5:\"title\";s:6:\"title1\";s:7:\"web_url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:26:\"<p>this is description</p>\";s:5:\"price\";s:2:\"46\";s:2:\"da\";s:3:\"102\";s:2:\"fb\";s:2:\"10\";s:8:\"follower\";s:2:\"10\";s:7:\"blog_id\";s:1:\"4\";s:7:\"user_id\";s:1:\"3\";}}', 'a:9:{s:6:\"_token\";s:40:\"Doeh3SqRBX0eu4COE2EvwNq6XpH8krkAYtZcFXZ0\";s:5:\"fname\";s:3:\"abc\";s:5:\"lname\";s:3:\"abc\";s:7:\"address\";s:3:\"abc\";s:4:\"city\";s:3:\"abc\";s:7:\"country\";s:3:\"abc\";s:9:\"post_code\";s:3:\"abc\";s:12:\"phone_number\";s:3:\"abc\";s:5:\"notes\";s:3:\"abc\";}', '2019-12-23 06:56:04', '2019-12-23 06:56:04', 246, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `order_blogs`
--

CREATE TABLE `order_blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'add user', 'web', '2019-07-26 06:01:49', '2019-07-26 06:01:49'),
(2, 'Administer roles & permissions', 'web', '2019-07-29 09:51:55', '2019-07-29 09:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2019-07-26 06:03:22', '2019-07-26 06:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `credit`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '2019-12-17 11:38:07', '$2y$10$CIX0RD3NNo75/aBkDlNaFedNvEuLuDXpLE7MXZbGy.Ga00PEWX/H6', 0, NULL, '2019-07-22 09:25:57', '2019-07-26 06:17:40'),
(2, 'asad', 'asad@example.com', '2019-07-29 09:45:29', '$2y$10$nbq27n9AtAs0Tsv3SwneNOAb/gJz9act826EyrulLaX3GOR.bUGJy', 0, NULL, '2019-07-29 04:45:29', '2019-07-29 04:45:29'),
(3, 'ahsan', 'ahsan.amin334@gmail.com', '2019-12-17 10:57:22', '$2y$10$CIX0RD3NNo75/aBkDlNaFedNvEuLuDXpLE7MXZbGy.Ga00PEWX/H6', 0, NULL, '2019-12-17 05:57:22', '2019-12-17 05:57:22');

-- --------------------------------------------------------

--
-- Table structure for table `user_blogs`
--

CREATE TABLE `user_blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `blogs_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_metas`
--

CREATE TABLE `user_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_industries`
--
ALTER TABLE `blog_industries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_industries_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_industries_industry_id_foreign` (`industry_id`);

--
-- Indexes for table `blog_keywords`
--
ALTER TABLE `blog_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_keywords_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_keywords_keyword_id_foreign` (`keyword_id`);

--
-- Indexes for table `blog_metas`
--
ALTER TABLE `blog_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_metas_blog_id_foreign` (`blog_id`);

--
-- Indexes for table `credit_histories`
--
ALTER TABLE `credit_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `credit_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_blogs`
--
ALTER TABLE `order_blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_blogs_order_id_foreign` (`order_id`),
  ADD KEY `order_blogs_blog_id_foreign` (`blog_id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_blogs`
--
ALTER TABLE `user_blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_blogs_user_id_foreign` (`user_id`),
  ADD KEY `user_blogs_blogs_id_foreign` (`blogs_id`);

--
-- Indexes for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_metas_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `blog_industries`
--
ALTER TABLE `blog_industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog_keywords`
--
ALTER TABLE `blog_keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `blog_metas`
--
ALTER TABLE `blog_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `credit_histories`
--
ALTER TABLE `credit_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_blogs`
--
ALTER TABLE `order_blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_blogs`
--
ALTER TABLE `user_blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_metas`
--
ALTER TABLE `user_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_industries`
--
ALTER TABLE `blog_industries`
  ADD CONSTRAINT `blog_industries_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_industries_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_keywords`
--
ALTER TABLE `blog_keywords`
  ADD CONSTRAINT `blog_keywords_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_keywords_keyword_id_foreign` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_metas`
--
ALTER TABLE `blog_metas`
  ADD CONSTRAINT `blog_metas_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_blogs`
--
ALTER TABLE `order_blogs`
  ADD CONSTRAINT `order_blogs_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_blogs_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_blogs`
--
ALTER TABLE `user_blogs`
  ADD CONSTRAINT `user_blogs_blogs_id_foreign` FOREIGN KEY (`blogs_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_metas`
--
ALTER TABLE `user_metas`
  ADD CONSTRAINT `user_metas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
