<?php

return array(
    /** set your paypal credential **/
    'client_id' =>'Aeu5pvA04L7sevJ8pLjZYEKWC_0KCfXC5QYLShpcXxM_VSPxLR1hSItj59yCQns8OaDDPoFiNATy3Gme',
    'secret' => 'EETU8yKZwa__Zcaq6xC_vi0zOq7uvODuEUdb1NCIcYiL_VTBr-zPa8YGhv_SejGp4hLlp8LH_EyPs7nK',
    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 1000,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
